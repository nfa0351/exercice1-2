package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;


/**
 * Classe abstraite qui implémente l'interface ImageStreamingDeserializer
 * @param <M>
 */
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
    @Override
    public void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}

