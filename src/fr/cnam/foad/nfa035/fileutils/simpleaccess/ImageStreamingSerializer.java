package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Classe interface de sérialisation
 * @param <S>
 * @param <M>
 */
public interface ImageStreamingSerializer<S, M> {
    void serialize(S source, M media) throws IOException;
    <K extends InputStream> K getSourceInputStream(S source) throws IOException;
    <T extends OutputStream> T getSerializingStream(M media) throws IOException;
}


