package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémente la classe abstraite AbstractStreamingImageDeserializer
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer {

    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
    }

    @Override
    public void deserialize(Object media) throws IOException {
    }

    @Override
    public OutputStream getSourceOutputStream() {
        return getSourceOutputStream();
    }

    @Override
    public InputStream getDeserializingStream(Object media) throws IOException {
        return getDeserializingStream(media);
    }
}
