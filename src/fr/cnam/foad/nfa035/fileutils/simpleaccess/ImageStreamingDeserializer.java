package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Classe interface de désérialisation
 * @param <M>
 */
public interface ImageStreamingDeserializer<M> {
    void deserialize(M media) throws IOException;
    <K extends InputStream> K getDeserializingStream(M media) throws IOException;
    <T extends OutputStream> T getSourceOutputStream();
}



