package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implémente la classe abstraite AbstractStreamingImageSerializer
 */
public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer {

    public ImageSerializerBase64StreamingImpl() {
    }

    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {
        return getSourceInputStream(source);
    }

    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return getSerializingStream(media);
    }

    @Override
    public void serialize(Object source, Object media) throws IOException {

    }
}
