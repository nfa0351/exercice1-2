package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;

/**
 * Classe abstraite qui implémente l'interface ImageStreamingSerializer
 * @param <S>
 * @param <M>
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {

}
