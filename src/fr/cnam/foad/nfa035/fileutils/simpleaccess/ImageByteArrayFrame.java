package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implementation de la classe abstraite AbstractImageFrameMedia
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia {
    public ImageByteArrayFrame(ByteArrayOutputStream byteArrayOutputStream) {
    }

    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        return getEncodedImageOutput();
    }

    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return getEncodedImageInput();
    }
}
